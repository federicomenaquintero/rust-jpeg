// Decoder state
//
// Comes from jpegint.h - DSTATE_*
enum State {
    Start,      // after create_decompress
    InHeader,   // reading header markers, no SOS yet
    Ready,      // found SOS, ready for start_decompress
    Preload,    // reading multiscan file in start_decompress
    Prescan,    // performing dummy pass for 2-pass quant
    Scanning,   // start_decompress done, read_scanlines OK
    RawOk,      // start_decompress done, read_raw_data OK
    BufImage,   // expecting jpeg_start_output
    BufPost,    // looking for SOS/EOI in jpeg_finish_output
    ReadCoeffs, // reading file in jpeg_read_coefficients
    Stopping,   // looking for EOI in jpeg_finish_decompress
}
