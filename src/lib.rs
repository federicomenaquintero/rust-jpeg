//! A JPEG codec.
//!
//! This is an experiment in writing a new JPEG codec in Rust with the
//! following features:
//!
//! * Uses the SIMD functions from libjpeg-turbo.
//! * Has easy "encode me a JPEG" and "decode me a JPEG" APIs.
//! * Has detailed APIs to build custom encoding and decoding pipelines.

#![allow(unused)]

pub mod common;
mod decoder;
mod encoder;

pub use common::color_convert::{RgbToYcc, RgbYccRust};
pub use common::density::{Density, Units};
pub use common::marker::{Marker, MarkerType};
pub use common::types::{Sample, Sample12, Sample8};

pub fn foo() {
    let conv = RgbYccRust::<Sample8>::new();

    let x = vec![1u8; 3000];
    let mut a = vec![0u8; 1000];
    let mut b = vec![0u8; 1000];
    let mut c = vec![0u8; 1000];

    conv.rgb_to_ycc(&x[..], &mut a[..], &mut b[..], &mut c[..]);

    println!("{:?}{:?}{:?}", a, b, c);
}
