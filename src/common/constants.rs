// These come from jpeglib.h
//
// All of these are specified by the JPEG standard, so don't change them
// if you want to be compatible.

/// Block size for Discrete Cosine Transform (DCT).
///
/// Each block is 8x8 samples.
pub const DCT_SIZE: usize = 8;

/// [`DCT_SIZE`] squared, number of elements in a block.
///
/// [`DCT_SIZE`]: const.DCT_SIZE.html
pub const DCT_SIZE_SQUARED: usize = DCT_SIZE * DCT_SIZE;

/// Number of quantization tables.
pub const NUM_QUANT_TABLES: usize = 4;

/// Number of Huffman tables.
pub const NUM_HUFF_TABLES: usize = 4;

/// Number of arithmetic coding tables.
pub const NUM_ARITH_TABLES: usize = 16;

/// Maximum number of components per scan.
pub const MAX_COMPONENTS_IN_SCAN: usize = 4;

/// JPEG limit on sampling factors.
pub const MAX_SAMPLE_FACTOR: usize = 4;
