use std::mem;

/// Type for the value of a single color channel.
///
/// We want to support 8-bit and 12-bit JPEG images, so the rest of the code
/// should be generic on samples that implement this trait.
///
/// This is equivalent to `JSAMPLE`.
pub trait Sample {
    /// Maximum value, plus 1, that a sample can attain = 2^BITS.
    const RANGE_UPPER: usize; // MAXJSAMPLE + 1

    /// Maximum value that a sample can attain = 2^BITS - 1.
    const MAX: i32; // MAXJSAMPLE

    const CENTER: i32; // CENTERJSAMPLE
    const BITS: i32; // BITS_IN_JSAMPLE

    /// Maximum value for an entry in a quantization table.
    const QUANT_TABLE_MAX_VALUE: u16;

    /// For the DefineQuantizationTables marker; 0 for 8-bit, 1 for 16-bit (12-bit JPEG).
    const QUANT_TABLE_PRECISION: u8;

    /// Gets the sample's value as an `i32`.
    fn get(&self) -> i32; // GETJSAMPLE()

    /// Casts an i32 to a sample value by truncation
    fn from(x: i32) -> Self;
}

/// Type for 8-bit samples.  Most JPEG images will use this.
pub type Sample8 = u8;

impl Sample for Sample8 {
    const RANGE_UPPER: usize = 256;
    const MAX: i32 = 255;
    const CENTER: i32 = 128;
    const BITS: i32 = 8;
    const QUANT_TABLE_MAX_VALUE: u16 = 255;
    const QUANT_TABLE_PRECISION: u8 = 0;

    fn get(&self) -> i32 {
        i32::from(*self)
    }

    fn from(x: i32) -> Self {
        x as u8
    }
}

/// Type for 12-bit samples.
// Note that libjpeg-turbo uses short for 12-bit samples, but prefers unsigned char for
// 8-bit samples.  I don't know if the signedness makes a difference.
pub type Sample12 = i16;

impl Sample for Sample12 {
    const RANGE_UPPER: usize = 4096;
    const MAX: i32 = 4095;
    const CENTER: i32 = 2048;
    const BITS: i32 = 12;
    const QUANT_TABLE_MAX_VALUE: u16 = 32767;
    const QUANT_TABLE_PRECISION: u8 = 1;

    fn get(&self) -> i32 {
        i32::from(*self)
    }

    fn from(x: i32) -> Self {
        x as i16
    }
}

// jmemmgr.c:alloc_sarray() uses an ALIGN_SIZE constant which is also 32 in the case of
// SIMD, and allocates rows of samples that are multiples of 2 * ALIGN_SIZE bytes.  It
// says, "Since we are often upsampling with a factor 2, we align the size (not the start)
// to 2 * ALIGN_SIZE so that the upsampling routines don't have to be as careful about
// size."
//
// Also it seems that the SIMD functions actually write in ALIGN_SIZE chunks, even if this
// size is bigger than the nominal size of a sample row.  For example, a row with 17 RGB
// samples may cause 32 bytes to be written to each of the Y/Cb/Cr planes.  The SIMD functions
// do not make an effort to write the "exact" size needed in the output.
//
// For the Rust code, we will *not* round up to 2 * ALIGN_SIZE, but use the following
// ROW_SIZE_QUANTUM instead, which is the same as ALIGN_SIZE.  This will allow enough
// slack space for the SIMD functions from libjpeg-turbo to do their work.  When the time
// comes to write the upsampling code, we can be more careful about buffer sizes instead of
// just assuming that every buffer should be as if for upsampling.
const ROW_SIZE_QUANTUM: usize = 32;

/// Computes the number of samples needed in a row buffer so it is usable with SIMD functions.
///
/// See the comments around jmemmgr.c:alloc_sarray()
///
/// That function allocates a 2D rectangle, and each row is as wide as the number of
/// samples required, rounded up to a certain chunk size.  See the comment for
/// ROW_SIZE_QUANTUM above, and also to see how the row size computed by this function
/// is different from alloc_sarray().
///
/// Of course, this function does not allocate anything, and just computes a single
/// row's size in samples.
///
/// # Panics
///
/// Will panic if num_samples is 0.
pub fn samples_in_padded_row<T: Sample>(num_samples: usize) -> usize {
    let sample_size = mem::size_of::<T>();

    assert!(ROW_SIZE_QUANTUM % sample_size == 0);
    assert!(num_samples > 0);

    rounded_up(num_samples, ROW_SIZE_QUANTUM / sample_size)
}

/// `len` rounded up to the next multiple of `align`, i.e. `ceil(len/align)*align`
fn rounded_up(len: usize, align: usize) -> usize {
    // similar in spirit to jmemmgr.c:round_up_pow2(),
    // but wrapping logic from Rust's alloc::Layout::padding_needed_for()
    len.wrapping_add(align).wrapping_sub(1) & !align.wrapping_sub(1)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn row_padding() {
        assert_eq!(samples_in_padded_row::<Sample8>(1), 32);
        assert_eq!(samples_in_padded_row::<Sample8>(32), 32);
        assert_eq!(samples_in_padded_row::<Sample8>(33), 64);

        assert_eq!(samples_in_padded_row::<Sample12>(1), 16);
        assert_eq!(samples_in_padded_row::<Sample12>(16), 16);
        assert_eq!(samples_in_padded_row::<Sample12>(17), 32);
    }
}
