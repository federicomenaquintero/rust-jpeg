use std::io::{Cursor, Write};
use std::marker::PhantomData;
use std::mem;

use byteorder::{BigEndian, WriteBytesExt};
use cast::{i32, u16, u8};

use super::constants::DCT_SIZE_SQUARED;
use super::types::Sample;
use super::zigzag::zigzag_to_natural;

/// Coefficient quantizers in natural array order.
pub struct QuantTable<T: Sample + Sized> {
    pub values: [u16; DCT_SIZE_SQUARED],

    sample: PhantomData<T>,
}

impl<T: Sample + Sized> QuantTable<T> {
    /// Create a quantization table based on a linear scaling factor.
    ///
    /// Normally user-facing code should use [`from_quality`] instead.
    ///
    /// [`from_quality`]: #method.from_quality

    // JQUANT_TBL has a field UINT16 quantval[DCTSIZE2], but
    // jpeg_add_quant_table() takes a const unsigned int *basic_table
    // argument.  Here we take a base_values: &[u16] to match the
    // final table.  Note that in libjpeg-turbo, the basic tables
    // std_luminance_quant_tbl and std_chrominance_quant_tbl are
    // indeed arrays of unsigned int (per the function's prototype),
    // but all the values fit in 8-bit integers.

    pub fn from_base_table(
        base_values: &[u16; DCT_SIZE_SQUARED],
        scale_factor: i32,
    ) -> QuantTable<T> {
        // jpeg_add_quant_table()

        let mut values = [0; DCT_SIZE_SQUARED];

        let max = i32(<T as Sample>::QUANT_TABLE_MAX_VALUE);

        for i in 0..DCT_SIZE_SQUARED {
            let mut new_v = (i32(base_values[i]) * scale_factor + 50) / 100;

            // Limit the value to the valid range
            if new_v <= 0 {
                new_v = 1;
            } else if new_v > max {
                new_v = max;
            }

            values[i] = u16(new_v).unwrap();
        }

        QuantTable {
            values,
            sample: PhantomData,
        }
    }

    /// Create a quantization table from a quality setting.
    ///
    /// You can use [`STANDARD_LUMINANCE_QUANT_TABLE`] or
    /// [`STANDARD_CHROMINANCE_QUANT_TABLE`] for the default tables from the JPEG
    /// standard.
    ///
    /// [`STANDARD_LUMINANCE_QUANT_TABLE`]: const.STANDARD_LUMINANCE_QUANT_TABLE.html
    /// [`STANDARD_CHROMINANCE_QUANT_TABLE`]: const.STANDARD_CHROMINANCE_QUANT_TABLE.html
    ///
    /// # Panics
    ///
    /// Will panic if `quality` is not in the range [0, 100] (inclusive).
    pub fn from_quality(base_values: &[u16; DCT_SIZE_SQUARED], quality: i32) -> QuantTable<T> {
        assert!(quality >= 0 && quality <= 100);
        Self::from_base_table(base_values, quality_to_scale_factor(quality))
    }

    pub(crate) fn serialize(&self, buf: &mut [u8]) {
        assert!(buf.len() == mem::size_of::<T>() * DCT_SIZE_SQUARED);

        let mut cursor = Cursor::new(buf);

        for i in 0..DCT_SIZE_SQUARED {
            // The table entries must be emitted in zigzag order.
            let v = self.values[zigzag_to_natural(i)];

            match mem::size_of::<T>() {
                1 => cursor.write_all(&[u8(i).unwrap()]).unwrap(),

                2 => cursor.write_u16::<BigEndian>(v).unwrap(),

                _ => unreachable!(),
            }
        }
    }
}

// Convert a user-specified quality rating to a percentage scaling factor
// for an underlying quantization table, using our recommended scaling curve.
// The input 'quality' factor should be 0 (terrible) to 100 (very good).
fn quality_to_scale_factor(mut quality: i32) -> i32 {
    // jpeg_quality_scaling()

    // Safety limit on quality factor.  Convert 0 to 1 to avoid zero divide.
    if quality <= 0 {
        quality = 1;
    } else if quality > 100 {
        quality = 100;
    }

    // The basic table is used as-is (scaling 100) for a quality of 50.
    // Qualities 50..100 are converted to scaling percentage 200 - 2*Q;
    // note that at Q=100 the scaling is 0, which will cause QuantTable::from_base_table
    // to make all the table entries 1 (hence, minimum quantization loss).
    // Qualities 1..50 are converted to scaling percentage 5000/Q.

    if (quality < 50) {
        5000 / quality
    } else {
        200 - quality * 2
    }
}

// These are the sample quantization tables given in Annex K (Clause K.1) of
// Recommendation ITU-T T.81 (1992) | ISO/IEC 10918-1:1994.
// The spec says that the values given produce "good" quality, and
// when divided by 2, "very good" quality.

/// Standard quantization table for luminance, from the JPEG standard.
// std_luminance_quant_tbl
#[rustfmt::skip]
pub const STANDARD_LUMINANCE_QUANT_TABLE: [u16; DCT_SIZE_SQUARED] = [
  16,  11,  10,  16,  24,  40,  51,  61,
  12,  12,  14,  19,  26,  58,  60,  55,
  14,  13,  16,  24,  40,  57,  69,  56,
  14,  17,  22,  29,  51,  87,  80,  62,
  18,  22,  37,  56,  68, 109, 103,  77,
  24,  35,  55,  64,  81, 104, 113,  92,
  49,  64,  78,  87, 103, 121, 120, 101,
  72,  92,  95,  98, 112, 100, 103,  99,
];

/// Standard quantization table for chrominance, from the JPEG standard.
// std_chrominance_quant_tbl
#[rustfmt::skip]
pub const STANDARD_CHROMINANCE_QUANT_TABLE: [u16; DCT_SIZE_SQUARED] = [
  17,  18,  24,  47,  99,  99,  99,  99,
  18,  21,  26,  66,  99,  99,  99,  99,
  24,  26,  56,  99,  99,  99,  99,  99,
  47,  66,  99,  99,  99,  99,  99,  99,
  99,  99,  99,  99,  99,  99,  99,  99,
  99,  99,  99,  99,  99,  99,  99,  99,
  99,  99,  99,  99,  99,  99,  99,  99,
  99,  99,  99,  99,  99,  99,  99,  99,
];
