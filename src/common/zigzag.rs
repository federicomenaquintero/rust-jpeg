use cast::usize;

use super::constants::DCT_SIZE_SQUARED;

/// Returns the natural-order position of the i'th element in zigzag order.
///
/// libjpeg-turbo has 16 extra entries with a value of 63 in the `JPEG_NATURAL_ORDER`
/// array, which are used when `i > 63`.  This happens with corrupted data.  See the
/// comment for `jpeg_natural_order` in the libjpeg-turbo sources.
///
/// We may want to run some benchmarks on doing the check here, vs. doing it in
/// the Huffman decoder (where corrupted image data would produce out-of-bounds accesses
/// in the lookup table), vs. just letting Rust panic on out-of-bounds access plus a
/// check in the decoder.
#[inline(always)]
pub fn zigzag_to_natural(i: usize) -> usize {
    if i > 63 {
        63
    } else {
        usize(JPEG_NATURAL_ORDER[i])
    }
}

/// JPEG_NATURAL_ORDER[i] is the natural-order position of the i'th element of zigzag order.
///
/// libjpeg-turbo keeps this as an array of ints; we'll use array of
/// u8 to fit it in a cache line hopefully.
#[rustfmt::skip]
const JPEG_NATURAL_ORDER: [u8; DCT_SIZE_SQUARED] = [
     0,  1,  8, 16,  9,  2,  3, 10,
    17, 24, 32, 25, 18, 11,  4,  5,
    12, 19, 26, 33, 40, 48, 41, 34,
    27, 20, 13,  6,  7, 14, 21, 28,
    35, 42, 49, 56, 57, 50, 43, 36,
    29, 22, 15, 23, 30, 37, 44, 51,
    58, 59, 52, 45, 38, 31, 39, 46,
    53, 60, 61, 54, 47, 55, 62, 63,
];
