use criterion::{criterion_group, criterion_main, Criterion};

use jpeg_rust::common::color_convert::{RgbToYcc, RgbYccNoSimd, RgbYccRust, RgbYccSimd};
use jpeg_rust::common::types::{samples_in_padded_row, Sample8};

fn bench_rgb_ycc_rust_sample8(c: &mut Criterion) {
    c.bench_function("RgbYccRust<Sample8>", |bencher| {
        let conv = RgbYccRust::new();

        let x = vec![1u8; 3000];
        let mut a = vec![0u8; 1000];
        let mut b = vec![0u8; 1000];
        let mut c = vec![0u8; 1000];

        bencher.iter(|| conv.rgb_to_ycc(&x[..], &mut a[..], &mut b[..], &mut c[..]))
    });
}

fn bench_nosimd_rgb_ycc(c: &mut Criterion) {
    c.bench_function("RgbYccNoSimd (Sample8)", |bencher| {
        let conv = RgbYccNoSimd::new();

        let x = vec![1 as Sample8; 3000];

        let mut row_a = vec![0 as Sample8; 1000];
        let mut row_b = vec![0 as Sample8; 1000];
        let mut row_c = vec![0 as Sample8; 1000];

        bencher.iter(|| conv.rgb_to_ycc(&x[..], &mut row_a[..], &mut row_b[..], &mut row_c[..]))
    });
}

fn bench_jsimd_rgb_ycc(c: &mut Criterion) {
    c.bench_function("RgbYccSimd (Sample8)", |bencher| {
        let conv = RgbYccSimd::new().expect("libjpeg-turbo cannot do SIMD RGB->YCbCr conversion");

        let x = vec![1 as Sample8; 3000];
        let num_padded_samples = samples_in_padded_row::<Sample8>(1000);

        let mut row_a = vec![0 as Sample8; num_padded_samples];
        let mut row_b = vec![0 as Sample8; num_padded_samples];
        let mut row_c = vec![0 as Sample8; num_padded_samples];

        bencher.iter(|| conv.rgb_to_ycc(&x[..], &mut row_a[..], &mut row_b[..], &mut row_c[..]))
    });
}

criterion_group!(
    benches,
    bench_rgb_ycc_rust_sample8,
    bench_nosimd_rgb_ycc,
    bench_jsimd_rgb_ycc
);

criterion_main!(benches);
